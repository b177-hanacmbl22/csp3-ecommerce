import	React, { Fragment } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import {Row, Col, Container} from 'react-bootstrap'; 
import slide1 from '../images/slide1.png';
import slide2 from '../images/slide2.png';
import slide3 from '../images/slide3.png';
import image1 from '../images/image1.png';
import './Home.css';


export default function Home(){

	return (
		
		<Container id="home">
			<Row className="heading">
				<Col id="home-title" className="text-center p-5">
					<h1 className="mb-3">SPECS | Eyeglasses</h1>
					<h4 className="mb-3"><i>Wear it</i> with <i><b>style</b></i>.</h4>
					<a href="/products" className="btn btn-outline-secondary">Browse Products</a>
				</Col>

				<Col className="img-fluid text-center p-5">
					
					<img src={image1} height={400} width={400} />
				</Col>
			</Row>

			<Carousel id="home-carousel">
					  <Carousel.Item>
					    <img
					      className="d-block w-100"
					      src={slide1}
					      alt="First slide"
					    />
					  </Carousel.Item>
					  <Carousel.Item>
					    <img
					      className="d-block w-100"
					      src={slide2}
					      alt="Second slide"
					    />
					  </Carousel.Item>
					  <Carousel.Item>
					    <img
					      className="d-block w-100"
					      src={slide3}
					      alt="Third slide"
					    />
					  </Carousel.Item>
					</Carousel>
			
		</Container>
	
	)
}