import React, {Fragment, useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from './../UserContext';
import './AppNavBar.css';

export default function AppNavBar() {
	const { user } = useContext(UserContext);
	

	return (
		<Navbar  fixed="top" id="navbar" expand="lg">
			<Navbar.Brand id="navbar-brand"  className="px-4" as={Link} to="/">SPECS</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/products">Products</Nav.Link>
					
					{(user.isAdmin === true) ?
						(user.accessToken !== null) ?
						<>
							<Nav.Link as={Link} to="/orders">Orders</Nav.Link>
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						</>
						:
						<>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>
					:
						(user.accessToken !== null) ?
						<>
							<Nav.Link as={Link} to="/checkout">Cart</Nav.Link>
							<Nav.Link as={Link} to="/history">Orders</Nav.Link>
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						</>
						:
						<>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>

					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};